//
//  Audiences+DataProvider.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-04-28.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension AudiencesInternal: DataProvider {

    // Called by NumberEight.Engine when data is to be removed.
    public func performDataRemoval() {
        NELog.msg(AudiencesInternal.LOG_TAG, debug: "Audiences is deleting its data")

        // No need to stop Insights - it will also delete its data and restart

        self.clearUserDefaults()
        AudiencesReporter.performDataRemoval()
        LiveAudiences.performDataRemoval()

        NELog.msg(AudiencesInternal.LOG_TAG, debug: "Data deleted.")
    }

    private func clearUserDefaults() {
        self.defaults.dictionaryRepresentation().keys.forEach { key in
            self.defaults.removeObject(forKey: key)
        }
        self.defaults.synchronize()
    }
}
