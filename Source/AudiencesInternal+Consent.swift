//
//  Audiences+Consent.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-04-28.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

// MARK: - Consentable
extension AudiencesInternal: Consentable {

    /// The list of consent used by NumberEight.Audiences
    public var requiredConsent: [NSNumber] {
        let req: [ConsentOptionsProperty] = [.ALLOW_PROCESSING, .ALLOW_STORAGE, .ALLOW_USE_FOR_AD_PROFILES, .ALLOW_USE_FOR_REPORTING]
        return req.map({ $0.rawValue}) as [NSNumber]
    }
}

// MARK: - Consent Change Listener
extension AudiencesInternal: OnConsentChangeListener {

    /// Delegate method to be notified when NumberEight consent changes.
    public func consentDidChange(_ consentOptions: ConsentOptions) {
        if consentOptions.hasRequiredConsent(self) {
            guard let prevToken = self.apiToken else { return }

            // Wait for the completion before moving on
            let group = DispatchGroup()
            group.enter()
            Audiences.startRecording(apiToken: prevToken) { _ in
                group.leave()
            }
            // Wait a max of 10 seconds
            switch group.wait(timeout: .now() + 10) {
            case .success:
                break
            case .timedOut:
                NELog.msg(AudiencesInternal.LOG_TAG, warning: "NumberEight Audiences consent restart timed out.")
            }
            NELog.msg(AudiencesInternal.LOG_TAG, info: "NumberEight Audiences now has consent to run, resuming.")
        } else {
            self._pauseRecording()
            NELog.msg(AudiencesInternal.LOG_TAG, info: "NumberEight Audiences no longer has appropriate consent, stopping.")
        }
    }
}
