//
//  AudiencesUsage.swift
//  Audiences
//
//  Created by Matthew Paletta on 2022-07-22.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

class AudiencesUsage: Encodable {
    var id: String
    var requests: Int64
    var age: Int64

    init(id: String, requests: Int64, age: Int64) {
        self.id = id
        self.requests = requests
        self.age = age
    }

    enum CodingKeys: String, CodingKey {
        case id
        case requests
        case age_days
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.requests, forKey: .requests)
        try container.encode(self.age, forKey: .age_days)
    }
}
