//
//  AudiencesDBHelper.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-22.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import SQLite

struct AudiencesDBHelper {
    private static let version: Int32 = 2
    private static let name = "audiences.db"
    private static let LOG_TAG = "AudiencesDBHelper"

    public static let AUDIENCE_REPORTING_TABLE = "audiencereporting"
    public static let AUDIENCE_AGES_TABLE = "audienceages"
    public static let AUDIENCE_DAYS_TABLE = "audiencedays"
    public static let MAX_AUDIENCE_DAYS_ENTRIES = 90

    static var connection: Connection? = {
        guard let loc = AudiencesDBHelper.location else {
            return nil
        }
        let result =
        Swift.Result { () -> Connection in
            try Connection(loc)
        }
        switch result {
        case let .success(conn):
            if AudiencesDBHelper.version != conn.userVersion {
                NELog.msg(AudiencesDBHelper.LOG_TAG,
                          info: "SQLite db version changed.", metadata: ["old": conn.userVersion ?? 1, "new": AudiencesDBHelper.version])
                conn.onVersionChange(newVersion: AudiencesDBHelper.version, oldVersion: (conn.userVersion ?? 1))
            }
            conn.onCreate()
            return conn
        case let .failure(error):
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Failed to get connection.", metadata: ["error": error])
            return nil
        }
    }()

    private static var location: Connection.Location? = {
        let result = Swift.Result { () -> String in
            try FileManager
                .default
                .url(for: .applicationSupportDirectory,
                        in: .userDomainMask,
                        appropriateFor: nil,
                        create: true)
                .appendingPathComponent(AudiencesDBHelper.name)
                .absoluteString
        }
        switch result {
        case let .success(urlStr):
            return .uri(urlStr)
        case let .failure(error):
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Failed to get connection location.", metadata: ["error": error])
            return nil
        }
    }()

    ///
    /// Creates the SQLite table schema necessary for Audience Usage Reporting.
    ///
    /// - Note: This will only create the table if they don't already exist.  This is safe to call multiple times.
    ///
    /// - Returns: Boolean indicating if it succesfully created the tables (`audiencereporting` & `audienceages`)
    ///
    internal func createTable() -> Bool {
        guard let db = AudiencesDBHelper.connection else {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Failed to acquire database connection.")
            return false
        }

        return AudiencesDBHelper.createTable(connection: db)
    }

    internal static func createTable(connection db: Connection) -> Bool {
        do {
            try db.transaction(.exclusive) {
                let audienceReportingStmt = try db.prepare("""
                CREATE TABLE IF NOT EXISTS \(AudiencesDBHelper.AUDIENCE_REPORTING_TABLE) (
                    date TEXT NOT NULL,
                    audience_id TEXT NOT NULL,
                    count INTEGER NOT NULL,
                    age INTEGER NOT NULL,
                    PRIMARY KEY (date, audience_id)
                )
                """)

                try audienceReportingStmt.run()

                let audienceAgesStmt = try db.prepare("""
                CREATE TABLE IF NOT EXISTS \(AudiencesDBHelper.AUDIENCE_AGES_TABLE) (
                    audience_id TEXT NOT NULL,
                    first_acquired TEXT NOT NULL,
                    deleted_at TEXT,
                    PRIMARY KEY (audience_id)
                )
                """)

                try audienceAgesStmt.run()

                let audienceDaysStmt = try db.prepare("""
                CREATE TABLE IF NOT EXISTS \(AudiencesDBHelper.AUDIENCE_DAYS_TABLE) (
                    date TEXT NOT NULL,
                    uuid TEXT NOT NULL,
                    uploaded_at TEXT,
                    PRIMARY KEY (date)
                )
                """)

                try audienceDaysStmt.run()
            }

#if DEBUG
            db.trace {
                NELog.msg(AudiencesDBHelper.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "An SQLite error occured while creating table.", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Error while creating table.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    private func dropTable() -> Bool {
        guard let db = AudiencesDBHelper.connection else {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Failed to acquire database connection.")
            return false
        }

        return AudiencesDBHelper.dropTable(connection: db)
    }

    internal static func dropTable(connection db: Connection) -> Bool {
        do {
            try db.transaction(.exclusive) {
                let audienceReportingStmt = try db.prepare("DROP TABLE IF EXISTS \(AudiencesDBHelper.AUDIENCE_REPORTING_TABLE)")
                try audienceReportingStmt.run()

                let audienceAgesStmt = try db.prepare("DROP TABLE IF EXISTS \(AudiencesDBHelper.AUDIENCE_AGES_TABLE)")
                try audienceAgesStmt.run()

                let audienceDaysStmt = try db.prepare("DROP TABLE IF EXISTS \(AudiencesDBHelper.AUDIENCE_DAYS_TABLE)")
                try audienceDaysStmt.run()
            }

#if DEBUG
            db.trace {
                NELog.msg(AudiencesDBHelper.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "An SQLite error occured while dropping table.", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Error while dropping table.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    internal func clearAllData() -> Bool {
        guard let db = AudiencesDBHelper.connection else {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Failed to acquire database connection.")
            return false
        }

        do {
            try db.transaction(.exclusive) {
                let audienceReportingStmt = try db.prepare("DELETE FROM \(AudiencesDBHelper.AUDIENCE_REPORTING_TABLE)")
                try audienceReportingStmt.run()

                let audienceAgesStmt = try db.prepare("DELETE FROM \(AudiencesDBHelper.AUDIENCE_AGES_TABLE)")
                try audienceAgesStmt.run()

                let audienceDaysStmt = try db.prepare("DELETE FROM \(AudiencesDBHelper.AUDIENCE_DAYS_TABLE)")
                try audienceDaysStmt.run()
            }

#if DEBUG
            db.trace {
                NELog.msg(AudiencesDBHelper.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "An SQLite error occured while clearing all data.", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(AudiencesDBHelper.LOG_TAG, error: "Error while dropping data in table.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    internal static func randomUUID() -> String {
        return UUID().uuidString
    }
}

fileprivate extension Connection {
    private static let LOG_TAG = "Connection"

    private(set) var userVersion: Int32? {
        get {
            let result = Swift.Result { () -> Int32? in
                guard let v = try scalar("PRAGMA user_version") as? Int64 else {
                    return nil
                }
                return Int32(v)
            }
            switch result {
            case let .success(uv):
                return uv
            case let .failure(error):
                NELog.msg(Connection.LOG_TAG, error: "Failed to get user version from connection", metadata: ["error": error.localizedDescription])
                return nil
            }
        }
        set {
            guard let nv = newValue else {
                return
            }
            do {
                try run("PRAGMA user_version = \(nv)")
            } catch {
                NELog.msg(Connection.LOG_TAG, error: "Failed to set user version for connection", metadata: ["error": error.localizedDescription])
            }
        }
    }

    func onCreate() {
        _ = AudiencesDBHelper.createTable(connection: self)
    }

    func onVersionChange(newVersion: Int32, oldVersion: Int32) {
        var updateSuccess = false
        if newVersion >= 2 && oldVersion < 2 {
            do {
                let stmt = try self.prepare("""
                    SELECT DISTINCT date FROM \(AudiencesDBHelper.AUDIENCE_REPORTING_TABLE);
                """)

    #if DEBUG
                self.trace {
                    NELog.msg(Connection.LOG_TAG, verbose: "DB Trace: \($0)")
                }
    #endif

                var days: [String] = []
                while let row = try stmt.failableNext() {
                    guard let date = row[0] as? String else { continue }
                    days.append(date)
                }

                _ = AudiencesDBHelper.createTable(connection: self)

                try self.transaction(.exclusive) {
                    let insertStmt = try self.prepare("""
                        INSERT INTO \(AudiencesDBHelper.AUDIENCE_DAYS_TABLE) (date, uuid) VALUES (?1, ?2);
                    """)
                    for day in days {
                        try insertStmt.run([day, AudiencesDBHelper.randomUUID()])
                    }
                }
                updateSuccess = true
    #if DEBUG
                self.trace {
                    NELog.msg(Connection.LOG_TAG, verbose: "DB Trace: \($0)")
                }
    #endif
            } catch let Result.error(message, code, statement) {
                NELog.msg(Connection.LOG_TAG, error: "An SQLite error occured while upgrading version", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            } catch let error {
                NELog.msg(Connection.LOG_TAG, error: "An error while upgrading versions.", metadata: ["error": error.localizedDescription])
            }
        }

        if !updateSuccess {
            _ = AudiencesDBHelper.dropTable(connection: self)
            _ = AudiencesDBHelper.createTable(connection: self)
        }

        self.userVersion = newVersion
    }
}
