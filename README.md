## Build instructions
This project is built with Xcode.

[![pipeline status](https://gitlab.com/numbereight/community/AudiencesSDKiOS/badges/master/pipeline.svg)](https://gitlab.com/numbereight/community/AudiencesSDKiOS/-/commits/master)

## Maintainers

* [Matthew Paletta](matt@numbereight.ai)
* [Chris Watts](chris@numbereight.me)
